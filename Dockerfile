FROM pytorch/pytorch

ENV DEBIAN_FRONTEND=noninteractive 
ENV TZ=Europe/Prague

EXPOSE 8888

RUN apt-get update && apt-get install -y vim coreutils python3-notebook python3-pip nano git libxrender1 wget

COPY . /protein-db
WORKDIR /protein-db

RUN git clone https://github.com/DeepGraphLearning/GearNet.git alphafold-protein-pipeline/GearNet
RUN wget https://zenodo.org/record/7593637/files/mc_gearnet_edge.pth
RUN mkdir alphafold-protein-pipeline/models
RUN mv mc_gearnet_edge.pth alphafold-protein-pipeline/models/mc_gearnet_edge.pth

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN addgroup --gid 1000 user && adduser --gid 1000 --uid 1000 --disabled-password --gecos user user
USER root
RUN chown -R user:user /protein-db
RUN chmod 755 /protein-db
USER user

CMD jupyter-lab --no-browser --ip "0.0.0.0" --port 8888 --NotebookApp.password=''
